import React from 'react'
import { GlassContainer } from '../../components/GlassContainer'
import { ShortenerServiceContext } from '../../services/shortener-service'

interface RedirectActionProps {
  uuid?: string
}

export const RedirectAction = ({ uuid }: RedirectActionProps) => {
  const shortenerService = React.useContext(ShortenerServiceContext)
  const [redirectUrl, setRedirectUrl] = React.useState('')
  const [isError, setIsError] = React.useState(false)
  const [errorMsg, setErrorMsg] = React.useState('')

  React.useEffect(() => {
    setIsError(false)
    setErrorMsg('')
    setRedirectUrl('')

    if (typeof uuid !== 'string') {
      setIsError(true)
      setErrorMsg('Invalid param in path')
      return
    }

    shortenerService.getShortenedUrl(uuid)
      .then(res => setRedirectUrl(res))
      .catch((e) => {
        setIsError(true)
        setErrorMsg(e.message)
      })
  }, [uuid, shortenerService])

  if (!isError && redirectUrl) {
    window.location.replace(redirectUrl)
  }

  if (isError) {
    return (
      <GlassContainer>
        <p>Cannot redirect due to "{errorMsg}"</p>
      </GlassContainer>
    )
  }

  return <GlassContainer>Loading...</GlassContainer>
}