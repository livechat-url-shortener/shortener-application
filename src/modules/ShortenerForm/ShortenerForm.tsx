import React from 'react'
import { FormWrapper, InputWrapper, ButtonWrapper, Message } from './styles'
import { Button, ButtonIcon } from '../../components/Button'
import { Input } from '../../components/Input'
import sendIcon from './../../assets/send.svg'
import { ShortenerServiceContext } from '../../services/shortener-service'

export const ShortenerForm = () => {
  const shortenerService = React.useContext(ShortenerServiceContext)
  const [inputText, setInputText] = React.useState('https://')
  const [isDisabled, setIsDisabled] = React.useState(false)
  const [isError, setIsError] = React.useState('')
  const [isSuccess, setIsSuccess] = React.useState('')

  const handleInputText = (text: string) => {
    setIsError('')
    setInputText(text)
  }

  const isValidUrl = React.useCallback(() => {
    return shortenerService.isValidUrl(inputText);
  }, [inputText, shortenerService]);

  const getButtonColor = React.useCallback(() => {
    if (isError.length > 0) return 'danger';
    return isValidUrl() ? 'info' : 'grey';
  }, [isValidUrl, isError])

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    setIsDisabled(true)

    shortenerService.shortenUrl(inputText).then(res => {
      setInputText('https://')
      setIsSuccess(res)
    }).catch(err => {
      setIsError(err.message)
    }).finally(() => {
      setIsDisabled(false)
    })
  }

  return (
    <FormWrapper onSubmit={onSubmit}>
      <InputWrapper>
        <Input value={inputText} onChange={handleInputText} />
        <Message>
          {isError && `Error happend: ${isError}`}
          {isSuccess && !isError && (
            <>
              Your shortened URL: <a href={`/${isSuccess}`}>/{isSuccess}</a>
            </>
          )}
        </Message>
      </InputWrapper>
      <ButtonWrapper>
        <Button 
          bgColor={getButtonColor()} 
          disabled={isError.length > 0 || isDisabled || !isValidUrl()} 
          hidden={!isValidUrl()}
        >
          <ButtonIcon src={sendIcon} />
        </Button>
      </ButtonWrapper>
    </FormWrapper>
  )
}