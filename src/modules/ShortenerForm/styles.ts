import styled, { css } from 'styled-components'

const flex = css`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  box-sizing: border-box;
`

export const InputWrapper = styled.div`
  ${flex};
  flex-direction: column;
  width: 100%;
`

export const ButtonWrapper = styled.div`
  ${flex};
  height: 100%;
`

export const FormWrapper = styled.form`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;

  width: 100%;
  box-sizing: border-box;

  & > ${InputWrapper} + ${ButtonWrapper} {
    margin-left: 15px;
  }
`

interface MessageProps {
  $isError?: boolean
  $isSuccess?: boolean
}

export const Message = styled.span<MessageProps>`
  height: 30px;
  font-size: 1.25rem;
  margin-top: 15px;
  ${props => props.$isError && css`
    color: var(--danger-color);
  `}
  ${props => props.$isSuccess && css`
    color: var(--success-color);
  `}

  transition: margin-top .15s ease-in-out;

  a {
    font-family: monospace;
    font-weight: bold;
    color: black;
  }

  &:empty {
    margin-top: -35px;
    opacity: 0;
    z-index: -1;
  }
`