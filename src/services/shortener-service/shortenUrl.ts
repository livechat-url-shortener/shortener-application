import { isValidUrl } from './isValidUrl'

export async function shortenUrl(url: string): Promise<string> {
  if (!isValidUrl(url)) {
    throw new Error(`URL ${url} is not a valid url`)
  }

  const response = await fetch('/v1/urls', {
    method: 'POST',
    body: JSON.stringify({ url }),
    headers: {
      'content-type': 'application/json',
    }
  })

  const body = await response.json()
  if (response.status !== 200) {
    throw new Error(body.detail)
  }

  return body.data.uuid;
}