export async function getShortenedUrl(uuid: string): Promise<string> {
  const response = await fetch(`/v1/urls/${uuid}`, {
    method: 'GET',
    headers: {
      'content-type': 'application/json',
    }
  })

  const body = await response.json()
  if (response.status !== 200) {
    throw new Error(body.detail)
  }

  return body.data.url;
}