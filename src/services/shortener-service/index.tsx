import React from "react"
import { getShortenedUrl } from './getShortenedUrl'
import { shortenUrl } from './shortenUrl'
import { isValidUrl } from './isValidUrl'

const contextValues = {
  getShortenedUrl,
  shortenUrl,
  isValidUrl
}

export const ShortenerServiceContext = React.createContext(contextValues)
export const ShortenerServiceProvider = ({ children }: { children?: React.ReactNode }) => (
  <ShortenerServiceContext.Provider value={contextValues}>
    {children}
  </ShortenerServiceContext.Provider>
)