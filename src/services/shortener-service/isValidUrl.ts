import Validator from "fastest-validator"

const validator = new Validator()
const validationSchema = { url: { type: 'url' }}

export function isValidUrl(url: string): boolean {
  return validator.validate({ url }, validationSchema) === true
}