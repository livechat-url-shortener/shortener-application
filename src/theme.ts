import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  * {
    box-sizing: border-box;
  }

  :root {
    --danger-color: rgba(255, 99, 87, 1);
    --warning-color: rgba(254, 188, 46, 1);
    --success-color: rgba(41, 206, 67, 1);
    --info-color: rgba(74, 196, 255, 1);
    --grey-color: rgba(85, 86, 86, 1);

    --danger-color-hover: rgba(255, 99, 87, 1);
    --warning-color-hover: rgba(254, 188, 46, 1);
    --success-color-hover: rgba(41, 206, 67, 1);
    --info-color--hover: rgba(74, 196, 255, 1);

    --radius-inside: 5px;
    --font-family: -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Ubuntu;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, Ubuntu;
  }
`