import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ThemeProvider } from 'styled-components'
import reportWebVitals from './reportWebVitals';
import { GlobalStyles } from './theme'
import { Background } from './components/Background'
import { FormPage } from './pages/FormPage';
import { RedirectPage } from './pages/RedirectPage';

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={{}}>
      <GlobalStyles />
      <Background>
        <BrowserRouter>
          <Switch>
            <Route path={'/:uuid'}>
              <RedirectPage />
            </Route>
            <Route path={'/'}>
              <FormPage />
            </Route>
          </Switch>
        </BrowserRouter>
      </Background>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals(console.info);
