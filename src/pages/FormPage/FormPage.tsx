import React from 'react'
import { GlassContainer } from '../../components/GlassContainer'
import { ShortenerForm } from '../../modules/ShortenerForm'
import { ShortenerServiceProvider } from '../../services/shortener-service'

export const FormPage = () => (
  <GlassContainer>
    <ShortenerServiceProvider>
      <ShortenerForm />
    </ShortenerServiceProvider>
  </GlassContainer>
)