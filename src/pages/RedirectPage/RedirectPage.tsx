import React from 'react'
import { useParams } from 'react-router-dom'
import { RedirectAction } from '../../modules/RedirectAction'
import { ShortenerServiceProvider } from '../../services/shortener-service'

interface UrlParamsInterface {
  uuid?: string
}

export const RedirectPage = () => {
  const { uuid } = useParams<UrlParamsInterface>()

  return (
    <ShortenerServiceProvider>
      <RedirectAction uuid={uuid} />
    </ShortenerServiceProvider>
  )
}