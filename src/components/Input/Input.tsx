import React from 'react'
import { FullWidthInput } from './styles'

interface InputProps {
  placeholderText?: string
  value: string
  onChange: (inputText: string) => void
}

export const Input = ({ placeholderText, value, onChange }: InputProps) => {
  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value)
  }

  return (
    <FullWidthInput 
      placeholder={placeholderText} 
      value={value} 
      onChange={handleInputChange}
    />
  )
}