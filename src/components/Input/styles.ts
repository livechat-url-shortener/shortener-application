import styled from 'styled-components'

export const FullWidthInput = styled.input`
  width: 100%;
  height: 48px;
  border: none;
  outline: none;

  background-color: rgb(217, 225, 245);
  box-shadow: 0px 2px 10px black, 0px 0px 1px white inset;
  border-radius: var(--radius-inside);
  padding: 12px 12px;
  transition: all 1s ease-out;

  font-family: var(--font-family);
  font-size: 1.25rem;

  color: rgb(71, 69, 67);
  ::placeholder {
    color: rgb(182, 182, 180);
  }
`