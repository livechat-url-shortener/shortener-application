import React from 'react'
import { StyledButton, IconImage } from './styles'

interface ButtonProps {
  children?: React.ReactNode
  bgColor?: 'warning' | 'danger' | 'success' | 'grey' | 'info'
  disabled?: boolean
  hidden?: boolean
}

interface ButtonIconProps {
  src: string
  iconColor?: 'black' | 'white'
}

export const Button = ({ children, bgColor = 'grey', disabled = false, hidden = false }: ButtonProps) => (
  <StyledButton $color={bgColor} $isHidden={hidden} disabled={disabled}>{children}</StyledButton>
)

export const ButtonIcon = ({ src, iconColor }: ButtonIconProps) => (
  <IconImage src={src} alt={''} />
)