import styled, { css } from 'styled-components'

interface StyledButtonProps {
  $color: 'warning' | 'danger' | 'success' | 'grey' | 'info'
  $isHidden?: boolean;
}

const varColor = (color: StyledButtonProps['$color']) => `var(--${color}-color)`

export const StyledButton = styled.button<StyledButtonProps>`
  border-radius: var(--radius-inside);
  font-family: var(--font-family);
  font-size: 1.25rem;
  height: 48px;

  background-color: ${props => varColor(props.$color)};
  box-shadow: 0px 2px 10px black, 0px 0px 1px white inset;

  border: none;
  outline: none;
  padding: 12px 14px;
  text-align: center;
  transition: all .15s ease-in-out;

  ${props => props.$isHidden && css`
    width: 0;
    margin: 0;
    padding: 0;

    > ${IconImage} {
      width: 0;
    }
  `}
`

export const IconImage = styled.img`
  width: 20px;
`