import styled from 'styled-components'

export const Glass = styled.div`
  background-color: rgba(255, 255, 255, .15);  
  backdrop-filter: blur(5px);
  box-sizing: border-box;
  display: flex;

  min-width: 250px;
  width: auto;
  border-radius: 10px;
  box-shadow: 0px 5px 10px black, 0px 0px 1px white inset;

  width: calc(100vw - 30px);
  padding: 40px 15px 20px;
  @media (min-width: 760px) {
    width: calc(100vw - 20%);
  }
  @media (min-width: 1024px) {
    width: 768px;
  }
`

const Dot = styled.div`
  width: 12px;
  height: 12px;
  border-radius: 100%;
`

export const RedDot = styled(Dot)`
  background-color: var(--danger-color);
`

export const YellowDot = styled(Dot)`
  background-color: var(--warning-color);
`

export const GreenDot = styled(Dot)`
  background-color: var(--success-color);
`

export const FakeNavBar = styled.aside`
  position: absolute;
  top: 10px;
  left: 10px;

  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;

  > ${Dot} + ${Dot} {
    margin-left: 8px;
  }
`