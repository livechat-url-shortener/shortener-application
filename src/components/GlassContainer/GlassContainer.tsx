import React from 'react'
import { Glass, FakeNavBar, RedDot, YellowDot, GreenDot } from './styles'

interface GlassContainerProps {
  children?: React.ReactNode
}

export const GlassContainer = ({ children }: GlassContainerProps) => (
  <Glass>
    <FakeNavBar>
      <RedDot />
      <YellowDot />
      <GreenDot />
    </FakeNavBar>
    {children}
  </Glass>
)