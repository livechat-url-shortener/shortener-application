import React from 'react'
import { FullScreenBackground } from './styles'

interface BackgroundProps {
  children?: React.ReactNode
  image?: string
}

export const Background = ({ children }: BackgroundProps) => (
  <FullScreenBackground>
    {children}
  </FullScreenBackground>
)