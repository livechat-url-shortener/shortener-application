import styled from 'styled-components'
import fallbackBg from '../../assets/full-screen-background.jpg'

interface FullScreenBackgroundProps {
  $url?: string;
}

export const FullScreenBackground = styled.div<FullScreenBackgroundProps>`
  width: 100vw;
  height: 100vh;

  margin: 0;
  padding: 0;

  display: flex;
  align-items: center;
  justify-content: center;

  background-image: url(${props => props.$url || fallbackBg});
  background-size: cover;
  background-repeat: no-repeat;
`