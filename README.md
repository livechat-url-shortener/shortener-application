# Shortener Application

**Shortener Application** is a simple application which allows to 
shorten your URL and redirect shortened URL to your target destination.

## Application

### Technology

Application is written with **React 17.0.1** and **TypeScript 4.0.5**. 
Application requires **shortener-service** to works.

### Infrastructure

![](./docs/infrastructure.png)

## Development

### Installation

* Copy the repository
* Copy the dependencies:

```bash
$ npm ci
```

### Configuration

Application requires **shortener service** to works on port `:8085`. If you have the need
to change the port, you need to change the proxy address in `package.json`.

### Running

```bash
$ npm run start
```

### Tests

